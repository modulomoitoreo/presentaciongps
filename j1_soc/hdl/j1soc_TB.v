`timescale 1ns / 1ps
`define SIMULATION
// ============================================================================
// TESTBENCH FOR TINYCPU
// ============================================================================

module j1soc_TB ();

reg 	sys_clk_i, sys_rst_i;
wire  	uart_tx, ledout; 
wire 	[15:0] gp_out0, gp_out1;
reg 	[15:0] gp_in0, gp_in1;
reg cond;
	reg [20:0] i;
	reg [20:0] fac;
reg uart_rx;

parameter BITRATE = 104166; 
j1soc uut (
	 uart_tx,uart_rx, ledout, gp_out0, gp_out1, gp_in0, gp_in1, sys_clk_i, sys_rst_i
);

initial begin


cond = 1;
  uart_rx=1;
  sys_clk_i   = 1;
  sys_rst_i = 1;
  #10 sys_rst_i = 0;
  gp_in0 = 16'd12;
  fac = 1;
  gp_in1 = 16'd465;
end

//always sys_clk_i = #5 ~sys_clk_i;   //100MHz
always sys_clk_i = #10 ~sys_clk_i;    //50MHz


initial begin: TEST_CASE
  $dumpfile("j1soc_TB.vcd");
  $dumpvars(-1, uut);
 #500000 
  send_car(8'h55); 
  send_car(8'h56);
  send_car(8'h55);
  send_car(8'h56);
  send_car(8'h24);
  send_car(8'h55);
  send_car(8'h56);
  #10000000 $finish;
 // #10000000 $finish;
end

////////////////////////////


task send_car;
    input [7:0] car;
  begin
    uart_rx <= 0;                 //-- Bit start 
    #BITRATE uart_rx <= car[0];   //-- Bit 0
    #BITRATE uart_rx <= car[1];   //-- Bit 1
    #BITRATE uart_rx <= car[2];   //-- Bit 2
    #BITRATE uart_rx <= car[3];   //-- Bit 3
    #BITRATE uart_rx <= car[4];   //-- Bit 4
    #BITRATE uart_rx <= car[5];   //-- Bit 5
    #BITRATE uart_rx <= car[6];   //-- Bit 6
    #BITRATE uart_rx <= car[7];   //-- Bit 7
    #BITRATE uart_rx <= 1;        //-- Bit stop
    #BITRATE uart_rx <= 1;        //-- Esperar a que se envie bit de stop
  end
  endtask



endmodule
