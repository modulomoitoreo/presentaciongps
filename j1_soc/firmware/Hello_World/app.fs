: delay_1ms
d# 8334 begin d# 1 - dup d# 0 = until \ 100MHz
;

: delay_ms
d# 0 do delay_1ms loop
;

: main

begin lee-uart h# 24 =  until
begin lee-uart h# 2c =  until
begin lee-uart h# 2c =  until
begin lee-uart h# 2c =  until
\ Lee Latitud
lee-uart d# 1 guarda-ram
lee-uart d# 2 guarda-ram
lee-uart d# 3 guarda-ram
lee-uart d# 4 guarda-ram
lee-uart d# 5 guarda-ram
lee-uart d# 6 guarda-ram
lee-uart d# 7 guarda-ram
lee-uart d# 8 guarda-ram
lee-uart d# 9 guarda-ram
begin lee-uart h# 2c =  until
\ Norte o sur
lee-uart d# 10 guarda-ram
begin lee-uart h# 2c =  until
\ Longitud
lee-uart d# 11 guarda-ram
lee-uart d# 12 guarda-ram
lee-uart d# 13 guarda-ram
lee-uart d# 14 guarda-ram
lee-uart d# 15 guarda-ram
lee-uart d# 16 guarda-ram
lee-uart d# 17 guarda-ram
lee-uart d# 18 guarda-ram
lee-uart d# 19 guarda-ram
lee-uart d# 20 guarda-ram
begin lee-uart h# 2c =  until
\ Este oeste
lee-uart d# 21 guarda-ram
begin lee-uart h# 2c =  until
\ Velocidad
lee-uart d# 22 guarda-ram
lee-uart d# 23 guarda-ram
lee-uart d# 24 guarda-ram
lee-uart d# 25 guarda-ram


\ Manda la palabra velocidad

h# 76 emit-uart \ v
h# 65 emit-uart \ e
h# 6c emit-uart \ l
h# 6f emit-uart \ o
h# 63 emit-uart \ c
h# 69 emit-uart \ i
h# 64 emit-uart \ d
h# 61 emit-uart \ a
h# 64 emit-uart \ d
h# 3a emit-uart \ : 
h# 20 emit-uart \ (espacio) 

\ Manda la velocidad
d# 22 lee-ram emit-uart
d# 23 lee-ram emit-uart
d# 24 lee-ram emit-uart
d# 25 lee-ram emit-uart

\ Retorno de carro
h# d emit-uart 
\ Salto de linea
h# a emit-uart

\ Manda la palabbra Latitud

h# 4c emit-uart \ L
h# 61 emit-uart \ a
h# 74 emit-uart \ t
h# 69 emit-uart \ i
h# 74 emit-uart \ t
h# 75 emit-uart \ u
h# 64 emit-uart \ d
h# 3a emit-uart \ : 
h# 20 emit-uart \ (espacio) 

\ Escribe la titud
d# 1 lee-ram emit-uart
d# 2 lee-ram emit-uart

h# 20 emit-uart \ (espacio) 
\ Escribe 'deg'
h# 64 emit-uart \ d 
h# 65 emit-uart \ e
h# 67 emit-uart \ g
h# 2e emit-uart \ .

h# 20 emit-uart \ (espacio) 
\ escribe los minutos

d# 3 lee-ram emit-uart
d# 4 lee-ram emit-uart
d# 5 lee-ram emit-uart
d# 6 lee-ram emit-uart
d# 7 lee-ram emit-uart
d# 8 lee-ram emit-uart
d# 9 lee-ram emit-uart
h# 20 emit-uart \ (espacio) 
\ escribe la palabra min.

h# 6d emit-uart \ m 
h# 69 emit-uart \ i
h# 6e emit-uart \ n
h# 2e emit-uart \ .
h# 20 emit-uart \ (espacio) 
\ Escribe norte o sur

d# 10 lee-ram emit-uart

\ Retorno de carro
h# d emit-uart 
\ Salto de linea
h# a emit-uart


\ Manda la palabbra Latitud

h# 4c emit-uart \ L
h# 61 emit-uart \ a
h# 74 emit-uart \ t
h# 69 emit-uart \ i
h# 74 emit-uart \ t
h# 75 emit-uart \ u
h# 64 emit-uart \ d
h# 3a emit-uart \ : 
h# 20 emit-uart \ (espacio) 

\ Escribe la titud
d# 11 lee-ram emit-uart
d# 12 lee-ram emit-uart
d# 13 lee-ram emit-uart

h# 20 emit-uart \ (espacio) 
\ Escribe 'deg'
h# 64 emit-uart \ d 
h# 65 emit-uart \ e
h# 67 emit-uart \ g
h# 2e emit-uart \ .

h# 20 emit-uart \ (espacio) 
\ escribe los minutos

d# 14 lee-ram emit-uart
d# 15 lee-ram emit-uart
d# 16 lee-ram emit-uart
d# 17 lee-ram emit-uart
d# 18 lee-ram emit-uart
d# 19 lee-ram emit-uart
h# 20 emit-uart \ (espacio) 
\ escribe la palabra min.

h# 6d emit-uart \ m 
h# 69 emit-uart \ i
h# 6e emit-uart \ n
h# 2e emit-uart \ .
h# 20 emit-uart \ (espacio) 
\ Escribe norte o sur

d# 21 lee-ram emit-uart

\ Retorno de carro
h# d emit-uart 
\ Salto de linea
h# a emit-uart


;
